package SZOB.Models;

/**
 * Created by Dominik on 2016-04-19.
 */
public class Room_Type {
    private String type_name;
    private float price;

    public Room_Type(){}

    public Room_Type(String type_name, float price){
        this.type_name=type_name;
        this.price=price;
    }

    public String getType_name(){
        return type_name;
    }

    public void setType_name(String type_name){
        this.type_name=type_name;
    }

    public float getPrice(){
        return price;
    }

    public void setPrice(float price){
        this.price=price;
    }

}
