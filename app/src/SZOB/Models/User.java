package SZOB.Models;

public class User {
    private int worker_id;
    private String password;
    private String name;
    private String last_name;
    private String e_mail;
    private String tel_number;
    private int role_id;

    public User(){}

    public User(int worker_id, String password, String name, String last_name, String e_mail, String tel_number, int role_id) {
        this.worker_id = worker_id;
        this.password = password;
        this.name = name;
        this.last_name = last_name;
        this.e_mail = e_mail;
        this.tel_number = tel_number;
        this.role_id = role_id;
    }

    public int getWorker_id() {
        return worker_id;
    }

    public void setWorker_id(int worker_id) {
        this.worker_id = worker_id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getE_mail() {
        return e_mail;
    }

    public void setE_mail(String e_mail) {
        this.e_mail = e_mail;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getTel_number() {
        return tel_number;
    }

    public void setTel_number(String tel_number) {
        this.tel_number = tel_number;
    }

    public int getRole_id() {
        return role_id;
    }

    public void setRole_id(int role_id) {
        this.role_id = role_id;
    }
}
