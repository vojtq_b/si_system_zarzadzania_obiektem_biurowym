package SZOB.ViewsControlers;

import SZOB.Models.User;
import SZOB.Models.Role;
import SZOB.ModelsControlers.DataValidator;
import SZOB.ModelsControlers.RoleController;
import SZOB.ModelsControlers.UserController;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;

import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.*;
import javafx.stage.Stage;


import java.util.List;
import java.util.ResourceBundle;

/**
 * Created by Wojtek on 2016-04-10.
 */
public class UserCreateDialogController implements Initializable {


    public TextField name_text;
    public TextField last_name_text;
    public TextField email_text;
    public TextField tel_nember_text;
    public TextField worker_id_text;
    public ChoiceBox role_box;
    public PasswordField password_text;
    Stage dialogStage;
    Role choosenRole = null;

    private ObservableList<Role> roleData = FXCollections.observableArrayList();

    public void initialize(java.net.URL location, ResourceBundle resources) {
        RoleController roleController = new RoleController();

        roleData = FXCollections.observableArrayList((List<Role>) roleController.listRoles());
        //role_box.set
        role_box.setItems(roleData);
        role_box.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Role>() {
            @Override
            public void changed(ObservableValue<? extends Role> observable, Role oldValue, Role newValue) {
                choosenRole = newValue;
            }
        });

    }


    public void save_button_click(ActionEvent actionEvent) throws Exception {

        if (DataValidator.validateLogin(worker_id_text.getText().trim()) == DataValidator.SUCCESS &&
                DataValidator.validateName(name_text.getText().trim()) == DataValidator.SUCCESS &&
                DataValidator.validateName(last_name_text.getText().trim()) == DataValidator.SUCCESS &&
                DataValidator.validateCellphoneNumber(tel_nember_text.getText().trim()) == DataValidator.SUCCESS &&
                DataValidator.validateEmail(email_text.getText().trim()) == DataValidator.SUCCESS &&
                DataValidator.validatePassword(password_text.getText()) == DataValidator.SUCCESS) {
            User user = new User();

            UserController userController = new UserController();
            user.setWorker_id(Integer.valueOf(worker_id_text.getText().trim()));
            user.setName(name_text.getText().trim());
            user.setLast_name(last_name_text.getText().trim());
            user.setE_mail(email_text.getText().trim());
            user.setTel_number(tel_nember_text.getText().trim());
            user.setRole_id(choosenRole.getRole_id());
            user.setPassword(password_text.getText());

            userController.addUser(user);
            dialogStage.close();
        }


    }

    public void cancel_button_click(ActionEvent actionEvent) throws Exception {
        dialogStage.close();
    }


    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }


}
