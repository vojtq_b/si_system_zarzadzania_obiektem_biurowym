package SZOB.Models;

import java.io.Serializable;

/**
 * Representation machines to get, set access parameters (id role and id module)
 */
public class Acces implements Serializable{
    private int role_id;
    private int module_id;

    /**
     * Constructor without parameters
     */
    public Acces() {
    }

    /**
     * Constructor with two parameters with possible ascribe role and module
     * @param role_id number access for users
     * @param module_id number of options activity
     */
    public Acces(int role_id, int module_id) {
        this.role_id = role_id;
        this.module_id = module_id;
    }

    /**
     * method for show role_id
     * @return number of role
     */
    public int getRole_id() {
        return role_id;
    }

    /**
     * method for assign role_id
     * @param role_id number of options activity
     */
    public void setRole_id(int role_id) {
        this.role_id = role_id;
    }

    /**
     * method for show module_id
     * @return number of options activity
     */
    public int getModule_id() {
        return module_id;
    }

    /**
     * method for assign module_id
     * @param module_id number of options activity
     */
    public void setModule_id(int module_id) {
        this.module_id = module_id;
    }

    @Override
    /**
     * method for checking whether there is a number of role and number of the module in the database
     */
    public boolean equals(Object b) {
        return (this.role_id==((Acces)b).getRole_id() && this.module_id==((Acces)b).module_id);
    }
}
