package SZOB.ViewsControlers;

import SZOB.Models.Reservation;
import SZOB.Models.Room;
import SZOB.ModelsControlers.DataValidator;
import SZOB.ModelsControlers.ReservationController;
import SZOB.ModelsControlers.RoomController;
import SZOB.ModelsControlers.UserController;
import SZOB.Reports.currentReportCreator;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Created by Wojciech Altenberg on 08.06.2016.
 */
public class ReservationEditDialogController implements Initializable {
    public Stage dialogStage;
    public Label room_number;
    public TextField tenant_name_text;
    public TextField tenant_id_text;
    public DatePicker start_date_picker;
    public DatePicker end_date_picker;

    public TextField price_text;
    public Button save_button;
    public Label period;
    public Label unit;
    private List<Reservation> reservations;
    private LocalDate margindate;
    private ObservableList<Room> roomList = FXCollections.observableArrayList();
    ReservationController reservationController = new ReservationController();
    private Room choosenRoom = null;

    //edytowana rezerwacja
    private Reservation edit_reservation=null;

    public Label room_area;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        if(edit_reservation==null) {

            start_date_picker.setValue(null);
            //setItemsDisable(true);

        }

        RoomController roomController = new RoomController();


        start_date_picker.getEditor().setDisable(true);
        end_date_picker.getEditor().setDisable(true);

        start_date_picker.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                updatereservations();
            }

        });
        end_date_picker.valueProperty().addListener(new ChangeListener<LocalDate>() {
            @Override
            public void changed(ObservableValue<? extends LocalDate> observable, LocalDate oldValue, LocalDate newValue) {
                if(newValue!=null) updatePeriodPriceInfo();
            }
        });
        start_date_picker.valueProperty().addListener(new ChangeListener<LocalDate>() {
            @Override
            public void changed(ObservableValue<? extends LocalDate> observable, LocalDate oldValue, LocalDate newValue) {
                updatereservations();
                getnearestStartDate();
                if(newValue==null){
                    end_date_picker.setDisable(true);
                }
                else {
                    end_date_picker.setDisable(false);

                }
                // end_date_picker.setValue(null);

                end_date_picker.setValue(start_date_picker.getValue());
            }
        });




        end_date_picker.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                updatereservations();
                getnearestStartDate();


            }
        });


        final Callback<DatePicker, DateCell> dayCellFactoryStart =
                new Callback<DatePicker, DateCell>() {

                    @Override
                    public DateCell call(final DatePicker datePicker) {
                        return new DateCell() {
                            @Override
                            public void updateItem(LocalDate item, boolean empty) {
                                super.updateItem(item, empty);


                                if (reservations != null) {
                                    for (Reservation x : reservations) {
                                        if (item.isAfter(LocalDate.parse(new SimpleDateFormat("yyyy-MM-dd").format(x.getStart_date())).minusDays(1)) &&
                                                item.isBefore(LocalDate.parse(new SimpleDateFormat("yyyy-MM-dd").format(x.getEnd_date())).plusDays(1))) {
                                            setDisable(true);

                                            setStyle("-fx-background-color: #ffc0cb;");

                                        }

                                    }
                                }
                                if (item.isBefore(LocalDate.now())) {
                                    setDisable(true);
                                    setStyle("-fx-background-color: #ffc0cb;");
                                }


                            }

                        };
                    }
                };


        start_date_picker.setDayCellFactory(dayCellFactoryStart);


        final Callback<DatePicker, DateCell> dayCellFactoryEnd =
                new Callback<DatePicker, DateCell>() {
                    @Override
                    public DateCell call(final DatePicker datePicker) {
                        return new DateCell() {


                            @Override
                            public void updateItem(LocalDate item, boolean empty) {
                                super.updateItem(item, empty);
                                if(start_date_picker.getValue()!=null) {
                                    if (item.isBefore(start_date_picker.getValue())) {
                                        setDisable(true);
                                        setStyle("-fx-background-color: #ffc0cb;");
                                    }

                                    if (margindate != null) {
                                        if (item.isAfter(margindate.minusDays(1))) {
                                            setDisable(true);
                                            setStyle("-fx-background-color: #ffc0cb;");

                                        }
                                    }

                                    if (ReservationManagementController.reservation_type == 1) {
                                        if (item.isAfter(start_date_picker.getValue().plusDays(30))) {
                                            setDisable(true);
                                            setStyle("-fx-background-color: #ffc0cb;");
                                        }
                                    }

                                }
                            }
                        };
                    }
                };


        //====================================================

        end_date_picker.setDayCellFactory(dayCellFactoryEnd);
        //end_date_picker.setValue(start_date_picker.getValue().plusDays(1));
        end_date_picker.setDisable(true);

    }


    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    public void updatereservations() {
        reservations = ReservationController.ListReservationInRoom(choosenRoom.getRoom_number());

    }

    public void getnearestStartDate() {
        margindate = null;
        LocalDate tmp;
        for (Reservation x : reservations) {
            if (LocalDate.parse(new SimpleDateFormat("yyyy-MM-dd").format(x.getStart_date())).isAfter(start_date_picker.getValue())) {
                tmp = LocalDate.parse(new SimpleDateFormat("yyyy-MM-dd").format(x.getStart_date()));
                if (margindate == null) {
                    margindate = tmp;
                }
                if (tmp.isBefore(margindate)) {
                    margindate = tmp;
                }
            }
        }

    }


    public void start_date_picker_click(ActionEvent actionEvent) throws Exception {
        updatereservations();

    }

    //public void setItemsDisable(boolean property) {
    //    tenant_name_text.setDisable(property);
    //    tenant_id_text.setDisable(property);
    //    start_date_picker.setDisable(property);
    //    end_date_picker.setDisable(property);

    //    price_text.setDisable(property);
    //}

    public Date localdateTodate(LocalDate ld) {

        if(ld==null){
            return null;
        }
        Instant instant = ld.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant();
        Date res = Date.from(instant);
        return res;
    }

    public LocalDate dateTolocaldate(Date date){
        if(date==null){
            return null;
        }
        return LocalDate.parse(new SimpleDateFormat("yyyy-MM-dd").format(date));
    }

    public void save_button_click(ActionEvent actionEvent) throws Exception {
        currentReportCreator rep = new currentReportCreator();

        if(DataValidator.validateTenantName(tenant_name_text.getText().trim())==DataValidator.SUCCESS &&
                DataValidator.validatePeselOrNip(tenant_id_text.getText().trim())==DataValidator.SUCCESS) {

            edit_reservation.setWorker_id(UserController.getActiveUser().getWorker_id());
            edit_reservation.setRoom_number(choosenRoom.getRoom_number());
            edit_reservation.setTenant_name(tenant_name_text.getText().trim());
            edit_reservation.setTenant_id(tenant_id_text.getText().trim());

            edit_reservation.setStart_date(localdateTodate(start_date_picker.getValue()));
            edit_reservation.setEnd_date(localdateTodate(end_date_picker.getValue()));
            edit_reservation.setReservation_type(ReservationManagementController.reservation_type);


            edit_reservation.setPrice(Float.valueOf(price_text.getText().trim()));

            if(edit_reservation==null) {

                //JOptionPane.showConfirmDialog();
                //rep.CreatePDF(tenant_name_text.getText(), start_date_picker.getValue(), end_date_picker.getValue(), price_text.getText(), choosenRoom.toString());
                reservationController.updateReservation(edit_reservation);
            }
            else{
                reservationController.updateReservation(edit_reservation);
            }

            dialogStage.close();


        }
    }

    public void cancel_button_click(ActionEvent actionEvent) throws Exception {
        dialogStage.close();
    }

    public void updatePeriodPriceInfo(){
        long duration=reservationController.getDuration(start_date_picker.getValue(),end_date_picker.getValue(),ReservationManagementController.reservation_type);
        period.setText(Long.toString(duration));

        if(ReservationManagementController.reservation_type==0){
            unit.setText("Miesięcy");
        }
        if(ReservationManagementController.reservation_type==1){
            unit.setText("Dni");
        }

        price_text.setText(Float.toString(Math.round(100.0F*reservationController.getTotalCost(duration,ReservationManagementController.reservation_type,choosenRoom))/100.0F));


    }


    public void setReservation(Reservation reservation){
        this.edit_reservation=reservation;

        choosenRoom = RoomController.getRoom(reservation.getRoom_number());
        room_area.setText(String.valueOf(choosenRoom.getRoom_area()));
        room_number.setText(String.valueOf(RoomController.getRoom(reservation.getRoom_number())));
        tenant_name_text.setText(reservation.getTenant_name());
        tenant_id_text.setText(reservation.getTenant_id());
        start_date_picker.setValue(dateTolocaldate(reservation.getStart_date()));
        end_date_picker.setValue(dateTolocaldate(reservation.getEnd_date()));
        period.setText(Long.toString(reservationController.getDuration(start_date_picker.getValue(),
                end_date_picker.getValue(),
                ReservationManagementController.reservation_type)));
        price_text.setText(Float.toString(reservation.getPrice()));
        updatereservations();
    }

}
