package SZOB.ViewsControlers;

import SZOB.Models.Room;
import SZOB.ModelsControlers.RoomController;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;

import javafx.scene.control.*;
import javafx.stage.Modality;
import javafx.stage.Stage;


import java.io.IOException;
import java.net.URL;

import java.time.LocalDate;
import java.util.Optional;
import java.util.ResourceBundle;

import SZOB.Models.Reservation;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.cell.PropertyValueFactory;

import javafx.collections.ObservableList;
import SZOB.ModelsControlers.ReservationController;
import javafx.collections.FXCollections;
/**
 * Created by Wojtek on 2016-04-16.
 */
public class ReservationManagementController implements Initializable{

    public TableView<Reservation> reservationsTable;
    public TableColumn<Reservation,String> collumn_reservation_id;
    public TableColumn<Reservation,String> collumn_tenant_name;
    public TableColumn<Reservation,String> collumn_start;
    public TableColumn<Reservation,String> collumn_end;
    public TableColumn<Reservation,String> collumn_room_number;

    private Reservation ChoosenReservation=null;

    private ObservableList<Reservation> reservationsData=FXCollections.observableArrayList();

    private ReservationController reservationController=new ReservationController();

    public static int reservation_type;

    public ComboBox<Room> combo_room_number;
    public TextField field_tenant_name;
    public DatePicker start_date;
    public DatePicker end_date;
    private ObservableList<Room> roomList = FXCollections.observableArrayList();
    private Room choosenRoom = null;

    @Override
    public void initialize(URL location, ResourceBundle resources){

        //lista rezerwacji
        collumn_reservation_id.setCellValueFactory(new PropertyValueFactory<Reservation,String>("reservation_id"));
        collumn_tenant_name.setCellValueFactory(new PropertyValueFactory<Reservation,String>("tenant_name"));
        collumn_start.setCellValueFactory(new PropertyValueFactory<Reservation,String>("start_date"));
        collumn_end.setCellValueFactory(new PropertyValueFactory<Reservation,String>("end_date"));
        collumn_room_number.setCellValueFactory(new PropertyValueFactory<Reservation,String>("room_number"));

        //wybrana rezerwacja
        reservationsTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Reservation>() {
            @Override
            public void changed(ObservableValue<? extends Reservation> observable, Reservation oldValue, Reservation newValue) {
                ChoosenReservation=newValue;
            }
        });

        //wyszukiwanie po nr pokoju
        RoomController roomController = new RoomController();

        roomList = FXCollections.observableArrayList(roomController.listRoom());
        roomList.add(new Room(0,0,"wszystkie"));
        combo_room_number.setItems(roomList);
        combo_room_number.getSelectionModel().selectLast();
        choosenRoom=combo_room_number.getValue();
        updateTable();

        combo_room_number.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Room>() {
            @Override
            public void changed(ObservableValue<? extends Room> observable, Room oldValue, Room newValue) {

                choosenRoom = newValue;
                updateTable();



            }
        });



        //wyszukiwanie po nazwie
        field_tenant_name.textProperty().addListener((observable, oldValue, newValue) -> {
            System.out.println("TextField Text Changed (newValue: " + newValue + ")");
            updateTable();
        });

        //wyszukiwanie po dacie
        start_date.valueProperty().addListener(new ChangeListener<LocalDate>() {
            @Override
            public void changed(ObservableValue<? extends LocalDate> observable, LocalDate oldValue, LocalDate newValue) {

                updateTable();
            }
        });


        end_date.valueProperty().addListener(new ChangeListener<LocalDate>() {
            @Override
            public void changed(ObservableValue<? extends LocalDate> observable, LocalDate oldValue, LocalDate newValue) {

                updateTable();
            }
        });
    }

    public void new_reservation_button_click(ActionEvent actionEvent) throws Exception{
        reservation_type=0;
        openReservationCreateDialog(actionEvent);


    }

    public void short_term_reservation_create_click(ActionEvent actionEvent) throws Exception{
        reservation_type=1;
        openReservationCreateDialog(actionEvent);
    }


    private void openReservationCreateDialog(ActionEvent actionEvent){
        Parent root;
        try {
            FXMLLoader loader =new FXMLLoader(getClass().getResource("../views/ReservationCreateDialog.fxml"));
            root = loader.load();
            Stage stage = new Stage();
            stage.setTitle("Dodawanie rezerwacji");
            stage.initOwner(((Node)(actionEvent.getSource())).getScene().getWindow());
            stage.initModality(Modality.WINDOW_MODAL);
            stage.setScene(new Scene(root));


            ReservationCreateDialogController reservationdialogcontroller = loader.getController();
            reservationdialogcontroller.setDialogStage(stage);
            stage.showAndWait();
            updateTable();


        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void openReservationEditDialog(ActionEvent actionEvent) {
        Parent root;
        try {
            FXMLLoader loader =new FXMLLoader(getClass().getResource("../views/ReservationEditDialog.fxml"));
            root = loader.load();
            Stage stage = new Stage();
            stage.setTitle("Edycja rezerwacji");
            stage.initOwner(((Node)(actionEvent.getSource())).getScene().getWindow());
            stage.initModality(Modality.WINDOW_MODAL);
            stage.setScene(new Scene(root));


            ReservationEditDialogController reservationdialogcontroller = loader.getController();
            reservationdialogcontroller.setReservation(ChoosenReservation);
            reservationdialogcontroller.setDialogStage(stage);
            stage.showAndWait();
            updateTable();


        } catch (IOException e) {
            e.printStackTrace();
        }

    }
	
	public void delete_reservation(ActionEvent actionEvent) throws Exception{
        delete_reservation_button_click(actionEvent);
    }

    public void delete_reservation_button_click(ActionEvent actionEvent) throws Exception{
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Uwaga");
        String s = "Czy na pewno chcesz usunąć wybraną rezerwację?";
        alert.setContentText(s);
        alert.setHeaderText(null);
        Optional<ButtonType> result = alert.showAndWait();

        if ((result.isPresent()) && (result.get() == ButtonType.OK)) {

            if (ChoosenReservation != null) {
                reservationController.deleteReservation(ChoosenReservation);
                updateTable();
            }
        }

    }

    public void updateTable(){
        ReservationCreateDialogController rcdc = new ReservationCreateDialogController();

        reservationsData = FXCollections.observableList(reservationController.ListReservationInRoomNameDate(choosenRoom.getRoom_number(),
                rcdc.localdateTodate(start_date.getValue()),rcdc.localdateTodate(end_date.getValue()),field_tenant_name.getText(),0));



        reservationsTable.setItems(reservationsData);
    }



}
