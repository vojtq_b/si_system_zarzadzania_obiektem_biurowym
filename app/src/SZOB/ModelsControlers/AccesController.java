package SZOB.ModelsControlers;

import SZOB.Models.Role;
import SZOB.Models.User;
import SZOB.Models.Acces;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 * Created by Wojtek on 2016-04-11.
 */
 
/**
 * Class below checks if the user has right to acces specific module, being given by his @param user login data and @param module number
 */
public class AccesController{

    public static boolean hasAcces(User user,int module){
		/**
		 * Session opens here, using @param getSessionFactory
		 */
        Session session = HibernateUtil.getSessionFactory().openSession();
		/**
		 * Setting three params to false/null, due to check if certain user is allowed to start action
		 */
        boolean exist =false;
        Transaction tx = null;
        Acces role=null;
        try{
			/**
			 * Try to begin @param tx transaction, if succesful, give access to user
			 */
            tx = session.beginTransaction();
            role=(Acces)session.get(Acces.class,new Acces(user.getRole_id(),module));

            tx.commit();
        }catch (HibernateException e) {
			/**
			 * If access denied, print out error
			 */
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
			/**
			 * After all, close session, reset to null the @param role, and set @param exist to true
			 */
            session.close();

            if(role!=null){
                exist=true;
            }

        }
		/**
		 * Return @param exist saying that user has tried to log in
		 */
        return exist;
    }
}
