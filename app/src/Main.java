
import SZOB.ModelsControlers.HibernateUtil;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class Main extends Application  {

    static Stage scene3;

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("/SZOB/views/Login.fxml"));
        primaryStage.setTitle("SYSTEM ZARZĄDZANIA OBIEKTEM BIUROWYM");
        primaryStage.setScene(new Scene(root));
        scene3=primaryStage;

        primaryStage.show();


    }

    public static void main(String[] args) {
        launch(args);
    }
}
