package SZOB.ModelsControlers;

import SZOB.Models.Reservation;
import SZOB.ViewsControlers.WarningDialogController;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static SZOB.ModelsControlers.ReservationController.ListReservationInRoom;

/**
 * Created by Wojciech Altenberg on 13.04.2016.
 */
 
 /**
 * Class below validates different kind of data, checking if it fulfils certain conditions
 */
public abstract class DataValidator {
    public static final int SUCCESS = 0;
    public static final int FAILURE = 1;

    public static final int SHORT_TERM_RESERVATION = 1;
    public static final int LONG_TERM_RESERVATION = 0;

    private static final int ERROR_INPUT_TOO_SHORT = 2;
    private static final int ERROR_FIELD_IS_EMPTY = 3;
    private static final int ERROR_SPACEBAR_IS_NOT_ALLOWED = 4;

    private static final int ERROR_NAME_START_WITH_UPPER_CASE_LETTER = 5;
    private static final int ERROR_NAME_NON_ALPHABETIC_SIGN_USED = 6;

    private static final int ERROR_LOGIN_NON_DIGIT_SIGN_USED = 7;
    private static final int ERROR_PASSWORD_NON_ALPHANUMERIC_SIGN_USED = 8;

    private static final int ERROR_EMAIL_AT_SIGN_NOT_USED = 9;
    private static final int ERROR_EMAIL_DOUBLE_AT_SIGN_USED = 10;
    private static final int ERROR_EMAIL_DOT_NOT_FOUND = 11;

    private static final int ERROR_CELLPHONE_IMPROPER_DATA_LENGTH = 12;
    private static final int ERROR_TOO_LONG_INPUT = 13;

    private static final int ERROR_PHONE_NUMBER_NON_DIGIT_SIGN_USED = 14;

    private static final int ERROR_PESEL_NIP_IMPROPER_DATA_LENGTH = 15;

    private static final int ERROR_PESEL_NIP_NON_DIGIT_SIGN_USED = 16;

    private static final int ERROR_TENANT_NAME_TOO_LONG = 17;

    private static final int ERROR_RESERVATION_TOO_LONG_TO_BE_SHORT_TERM = 18;
    private static final int ERROR_RESERVATION_TOO_SHORT_TO_BE_LONG_TERM = 19;
    private static final int ERROR_RESERVATION_OVERLAP = 20;

	/**
	 * Here, application checks if the @param name field is: not empty, not longer than 20 characters,
	 * starts with upper case letter, contains only alphabetic signs and does not contain space bars
	 */
    public static int validateName(String name) {
        if(name.length() == 0) {
            displayWarning(ERROR_FIELD_IS_EMPTY);
            return FAILURE;
        }
        if(name.length() > 20) {
            displayWarning(ERROR_TOO_LONG_INPUT);
            return FAILURE;
        }
        if(!Character.isUpperCase(name.charAt(0))) {
            displayWarning(ERROR_NAME_START_WITH_UPPER_CASE_LETTER);
            return FAILURE;
        }
        for(char c : name.toCharArray()) {
            if(!Character.isAlphabetic(c)) {
                displayWarning(ERROR_NAME_NON_ALPHABETIC_SIGN_USED);
                return FAILURE;
            }
            if(c==' ') {
                displayWarning(ERROR_SPACEBAR_IS_NOT_ALLOWED);
                return FAILURE;
            }
        }
        return SUCCESS;
    }
	/**
	 * Here, application checks if the @param login field is: not empty, not longer than 9 characters,
	 * contains only digits and does not contain space bars
	 */
    public static int validateLogin(String login) {
        if(login.length() == 0) {
            displayWarning(ERROR_FIELD_IS_EMPTY);
            return FAILURE;
        }
        if(login.length() > 9) {
            displayWarning(ERROR_TOO_LONG_INPUT);
            return FAILURE;
        }
        for(char c : login.toCharArray()) {
            if(!(Character.isDigit(c))) {
                displayWarning(ERROR_LOGIN_NON_DIGIT_SIGN_USED);
                return FAILURE;
            }
            if(c == ' ') {
                displayWarning(ERROR_SPACEBAR_IS_NOT_ALLOWED);
                return FAILURE;
            }
        }
        return SUCCESS;
    }
	/**
	 * Here, application checks if the @param password field is: not empty, not longer than 15 characters,
	 * contains only digits and alphabetic signs and does not contain space bars
	 */
    public static int validatePassword(String password) {
        if(password.length() == 0) {
            displayWarning(ERROR_FIELD_IS_EMPTY);
            return FAILURE;
        }
        if(password.length() > 15) {
            displayWarning(ERROR_TOO_LONG_INPUT);
            return FAILURE;
        }
        for(char c : password.toCharArray()) {
            if(!Character.isDigit(c) && !Character.isAlphabetic(c)) {
                displayWarning(ERROR_PASSWORD_NON_ALPHANUMERIC_SIGN_USED);
                return FAILURE;
            }
            if(c == ' ') {
                displayWarning(ERROR_SPACEBAR_IS_NOT_ALLOWED);
                return FAILURE;
            }
        }
        return SUCCESS;
    }
	/**
	 * Here, application checks if the @param email field is: not shorter than 5, and not longer than 50 characters,
	 * contains 'at' sign and 'dot' afterwards, verifies if 'at' is not duplicated, and does not contain space bars
	 */
    public static int validateEmail(String email){
        if(email.length() < 5) {
            displayWarning(ERROR_INPUT_TOO_SHORT);
            return FAILURE;
        }
        if(email.length() > 50) {
            displayWarning(ERROR_TOO_LONG_INPUT);
            return FAILURE;
        }
        boolean hasAt = false;
        boolean hasDot = false;
        for(char c: email.toCharArray()) {
            //check for double @ in text, and for single, search for dot after At sign, required
            if(c=='@' && hasAt) {
                displayWarning(ERROR_EMAIL_DOUBLE_AT_SIGN_USED);
                return FAILURE;
            } else if (c == '@' && !hasAt) {
                hasAt = true;
            } else if (hasAt && c == '.') {
                hasDot = true;
            }
            if(c == ' ') {
                displayWarning(ERROR_SPACEBAR_IS_NOT_ALLOWED);
                return FAILURE;
            }
        }
        if(!hasAt) {
            displayWarning(ERROR_EMAIL_AT_SIGN_NOT_USED);
            return FAILURE;
        } else  if(!hasDot) {
            displayWarning(ERROR_EMAIL_DOT_NOT_FOUND);
            return FAILURE;
        }
        return SUCCESS;
    }
	/**
	 * Here, application checks if the @param cellPhoneNumber field is: 9 characters long and contains only digits
	 */
    public static int validateCellphoneNumber(String number) {
        if(number.length() != 9) {
            displayWarning(ERROR_CELLPHONE_IMPROPER_DATA_LENGTH);
            return FAILURE;
        }
        for(char c : number.toCharArray()) {
            if(!Character.isDigit(c)) {
                displayWarning(ERROR_PHONE_NUMBER_NON_DIGIT_SIGN_USED);
                return FAILURE;
            }
        }
        return SUCCESS;
    }
	/**
	 * Here, application checks if the @param pesel field is: 10-11 characters long and contains only digits
	 */
    public static int validatePeselOrNip(String number){
        if(number.length() != 11 && number.length() != 10) {
            displayWarning(ERROR_PESEL_NIP_IMPROPER_DATA_LENGTH);
            return FAILURE;
        }
        for(char c : number.toCharArray()) {
            if(!Character.isDigit(c)) {
                displayWarning(ERROR_PESEL_NIP_NON_DIGIT_SIGN_USED);
                return FAILURE;
            }
        }
        return SUCCESS;
    }
	/**
	 * Here, application checks if the @param tenantName field is not longer than 255 characters
	 */
    public static int validateTenantName(String tenantName){
        if(tenantName.length() >= 255) {
            displayWarning(ERROR_TENANT_NAME_TOO_LONG);
            return FAILURE;
        }
        return SUCCESS;
    }
	/**
	 * Here, application checks if the reservation is: not too short to be a long term reservation,
	 *  not too long to be a short term reservation, and if the reservation doesnt't overlap existing one
	 */
    public static int validateReservation(int room_number, int type, LocalDate start, LocalDate end) {
        List<Reservation> roomReservations = ListReservationInRoom(room_number);

        long duration= ChronoUnit.DAYS.between(start,end);
        //long duration = Duration.between(start, end).toDays();

        if(type == SHORT_TERM_RESERVATION && duration >= 30) {
            displayWarning(ERROR_RESERVATION_TOO_LONG_TO_BE_SHORT_TERM);
            return FAILURE;
        } else if (type == LONG_TERM_RESERVATION && duration <= 30) {
            displayWarning(ERROR_RESERVATION_TOO_SHORT_TO_BE_LONG_TERM);
            return FAILURE;
        }

        for(Reservation reservation : roomReservations) {
            Instant startInstant = Instant.ofEpochMilli(reservation.getStart_date().getTime());
            LocalDate startReserved = LocalDateTime.ofInstant(startInstant, ZoneId.systemDefault()).toLocalDate();

            Instant endInstant = Instant.ofEpochMilli(reservation.getEnd_date().getTime());
            LocalDate endReserved = LocalDateTime.ofInstant(endInstant, ZoneId.systemDefault()).toLocalDate();

            if(startReserved.isBefore(start) && endReserved.isAfter(start)){
                displayWarning(ERROR_RESERVATION_OVERLAP);
            } else if(startReserved.isBefore(end) && endReserved.isAfter(end)) {
                displayWarning(ERROR_RESERVATION_OVERLAP);
            } else if(startReserved.isAfter(start) && endReserved.isBefore(end)) {
                displayWarning(ERROR_RESERVATION_OVERLAP);
            }
        }
        return SUCCESS;
    }
	/**
	 * Here are dialog messages stored
	 */
    private static void displayWarning(int exception_id) {
        if(exception_id == DataValidator.ERROR_INPUT_TOO_SHORT) {
            WarningDialogController.displayErrorDialog("Niewłaściwie wypełnione pole.\nJedno z pól jest zbyt krótkie.");
        }
        else if (exception_id == DataValidator.ERROR_FIELD_IS_EMPTY) {
            WarningDialogController.displayErrorDialog("Niewłaściwie wypełnione pole.\nJedno z pól jest puste.");
        }
        else if (exception_id == DataValidator.ERROR_SPACEBAR_IS_NOT_ALLOWED) {
            WarningDialogController.displayErrorDialog("Niewłaściwie wypełnione pole.\nNie wolno używać znaku spacji wewnątrz pól.");
        }
        else if (exception_id == DataValidator.ERROR_NAME_START_WITH_UPPER_CASE_LETTER) {
            WarningDialogController.displayErrorDialog("Niewłaściwie wypełnione pole.\nImię i nazwisko zaczynamy dużą literą.");
        }
        else if (exception_id == DataValidator.ERROR_NAME_NON_ALPHABETIC_SIGN_USED) {
            WarningDialogController.displayErrorDialog("Niewłaściwie wypełnione pole.\nImię i nazwisko mogą składać się" +
                    " jedynie ze znaków należących do alfabetu.");
        }
        else if (exception_id == DataValidator.ERROR_LOGIN_NON_DIGIT_SIGN_USED) {
            WarningDialogController.displayErrorDialog("Niewłaściwie wypełnione pole.\nLogin może zawierac jedynie cyfry.");
        }
        else if (exception_id == DataValidator.ERROR_PASSWORD_NON_ALPHANUMERIC_SIGN_USED) {
            WarningDialogController.displayErrorDialog("Niewłaściwie wypełnione pole.\nHasło może zawierać jedynie znaki alfanumeryczne.");
        }
        else if (exception_id == DataValidator.ERROR_EMAIL_AT_SIGN_NOT_USED) {
            WarningDialogController.displayErrorDialog("Niewłaściwie wypełnione pole.\nPole email nie zawiera znaku @.");
        }
        else if (exception_id == DataValidator.ERROR_EMAIL_DOUBLE_AT_SIGN_USED) {
            WarningDialogController.displayErrorDialog("Niewłaściwie wypełnione pole.\nPole email zawiera więcej niż jeden znak @.");
        }
        else if (exception_id == DataValidator.ERROR_EMAIL_DOT_NOT_FOUND) {
            WarningDialogController.displayErrorDialog("Niewłaściwie wypełnione pole.\nPole email nie ma kropki.");
        }
        else if (exception_id == DataValidator.ERROR_CELLPHONE_IMPROPER_DATA_LENGTH) {
            WarningDialogController.displayErrorDialog("Niewłaściwie wypełnione pole.\nNumer telefonu powinien zawierać 9 cyfr.");
        }
        else if (exception_id == DataValidator.ERROR_TOO_LONG_INPUT) {
            WarningDialogController.displayErrorDialog("Niewłaściwie wypełnione pole.\nZbyt długi łańcuch znaków w jednym z pól.");
        }
        else if (exception_id == DataValidator.ERROR_PHONE_NUMBER_NON_DIGIT_SIGN_USED) {
            WarningDialogController.displayErrorDialog("Niewłaściwie wypełnione pole.\nNumer telefonu może zawierać jedynie cyfry.");
        }
        else if (exception_id == DataValidator.ERROR_PESEL_NIP_NON_DIGIT_SIGN_USED) {
            WarningDialogController.displayErrorDialog("Niewłaściwie wypełnione pole.\nPESEL i NIP mogą zawierać jedynie cyfry.");
        }
        else if (exception_id == DataValidator.ERROR_PESEL_NIP_IMPROPER_DATA_LENGTH) {
            WarningDialogController.displayErrorDialog("Niewłaściwie wypełnione pole.\nPESEL powinien zawierać 11 cyfr, NIP 10 cyfr.");
        }
        else if (exception_id == DataValidator.ERROR_TENANT_NAME_TOO_LONG) {
            WarningDialogController.displayErrorDialog("Niewłaściwie wypełnione pole.\nNazwa najemcy zbyt długa.");
        }
        else if (exception_id == DataValidator.ERROR_RESERVATION_TOO_LONG_TO_BE_SHORT_TERM) {
            WarningDialogController.displayErrorDialog("Niewłaściwie wypełnione pole.\nRezerwacja zbyt długa na krótkoterminową.");
        }
        else if (exception_id == DataValidator.ERROR_RESERVATION_TOO_SHORT_TO_BE_LONG_TERM) {
            WarningDialogController.displayErrorDialog("Niewłaściwie wypełnione pole.\nRezerwacja zbyt krótka na długoterminową.");
        }
        else if (exception_id == DataValidator.ERROR_RESERVATION_OVERLAP) {
            WarningDialogController.displayErrorDialog("Niewłaściwie wypełnione pole.\nPrzedział czasu rezerwacji pokrywa się z inną rezerwacją.");
        }
    }
}
