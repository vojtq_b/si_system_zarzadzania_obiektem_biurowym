package SZOB.ModelsControlers;

import SZOB.Models.Role;
import SZOB.Models.User;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;


public class RoleController {

    public Role getRole(int id){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        Role role=null;
        try{
            tx = session.beginTransaction();
            role=(Role)session.get(Role.class,id);

            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {

            session.close();
            return role;
        }
    }

    public List<Role> listRoles( ){
        Session session = HibernateUtil.getSessionFactory().openSession();
        List roles=null;
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            roles = session.createQuery("FROM Role ").list();

            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }

        return roles;
    }



}
