package SZOB.ViewsControlers;

import SZOB.Models.Reservation;
import SZOB.Models.Room;
import SZOB.ModelsControlers.ReservationController;
import SZOB.ModelsControlers.RoomController;
import SZOB.Reports.archiveRaportCreator;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

/**
 * Created by Wojtek on 2016-04-16.
 */
public class ArchiveReservationController implements Initializable {

    public TableView<Reservation> reservationsTable;
    public TableColumn<Reservation, String> collumn_reservation_id;
    public TableColumn<Reservation, String> collumn_tenant_name;
    public TableColumn<Reservation, String> collumn_start;
    public TableColumn<Reservation, String> collumn_end;
    public TableColumn<Reservation, String> collumn_room_number;
    public TableColumn<Reservation, String> collumn_price;

    private Reservation ChoosenReservation = null;

    private ObservableList<Reservation> reservationsData = FXCollections.observableArrayList();

    private ReservationController reservationController = new ReservationController();

    public static int reservation_type;

    public ComboBox<Room> combo_room_number;
    public TextField field_tenant_name;
    public DatePicker start_date;
    public DatePicker end_date;
    private ObservableList<Room> roomList = FXCollections.observableArrayList();
    private Room choosenRoom = null;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        //lista rezerwacji
        collumn_reservation_id.setCellValueFactory(new PropertyValueFactory<Reservation, String>("reservation_id"));
        collumn_tenant_name.setCellValueFactory(new PropertyValueFactory<Reservation, String>("tenant_name"));
        collumn_start.setCellValueFactory(new PropertyValueFactory<Reservation, String>("start_date"));
        collumn_end.setCellValueFactory(new PropertyValueFactory<Reservation, String>("end_date"));
        collumn_room_number.setCellValueFactory(new PropertyValueFactory<Reservation, String>("room_number"));
        collumn_price.setCellValueFactory(new PropertyValueFactory<Reservation, String>("price"));
        //wybrana rezerwacja
        reservationsTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Reservation>() {
            @Override
            public void changed(ObservableValue<? extends Reservation> observable, Reservation oldValue, Reservation newValue) {
                ChoosenReservation = newValue;
            }
        });

        //wyszukiwanie po nr pokoju
        RoomController roomController = new RoomController();

        roomList = FXCollections.observableArrayList(roomController.listRoom());
        roomList.add(new Room(0, 0, "wszystkie"));
        combo_room_number.setItems(roomList);
        combo_room_number.getSelectionModel().selectLast();
        choosenRoom = combo_room_number.getValue();
        updateTable();

        combo_room_number.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Room>() {
            @Override
            public void changed(ObservableValue<? extends Room> observable, Room oldValue, Room newValue) {

                choosenRoom = newValue;
                updateTable();


            }
        });


        //wyszukiwanie po nazwie
        field_tenant_name.textProperty().addListener((observable, oldValue, newValue) -> {
            System.out.println("TextField Text Changed (newValue: " + newValue + ")");
            updateTable();
        });

        //wyszukiwanie po dacie
        start_date.valueProperty().addListener(new ChangeListener<LocalDate>() {
            @Override
            public void changed(ObservableValue<? extends LocalDate> observable, LocalDate oldValue, LocalDate newValue) {

                updateTable();
            }
        });


        end_date.valueProperty().addListener(new ChangeListener<LocalDate>() {
            @Override
            public void changed(ObservableValue<? extends LocalDate> observable, LocalDate oldValue, LocalDate newValue) {

                updateTable();
            }
        });
    }


    public void createDoc() throws IOException {

        archiveRaportCreator arc = new archiveRaportCreator();
        arc.CreatePDF(reservationsData);

    }


    public void updateTable() {
        ReservationCreateDialogController rcdc = new ReservationCreateDialogController();

        reservationsData = FXCollections.observableList(reservationController.ListReservationInRoomNameDate(choosenRoom.getRoom_number(),
                rcdc.localdateTodate(start_date.getValue()), rcdc.localdateTodate(end_date.getValue()), field_tenant_name.getText(), 1));


        reservationsTable.setItems(reservationsData);
    }


}
