package SZOB.ModelsControlers;

import SZOB.Models.Room_Type;
import SZOB.Models.User;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

/**
 * Created by Wojtek on 2016-04-20.
 */
public class Room_TypeController {

    public float getPrice(String type){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        Room_Type rt=null;
        try{
            tx = session.beginTransaction();
            rt=(Room_Type) session.get(Room_Type.class,type);

            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {

            session.close();
            return rt.getPrice();
        }
    }

    public void updateRoom_Type(Room_Type room_type){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;

        try{
            tx = session.beginTransaction();
            session.update(room_type);
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
    }

    public List<Room_Type> listRoom_Type( ){
        Session session = HibernateUtil.getSessionFactory().openSession();
        List room_types=null;
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            room_types = session.createQuery("FROM Room_Type").list();
            // for (Iterator iterator =
            //       users.iterator(); iterator.hasNext();){
            //     User user = (User) iterator.next();
            //     System.out.print("First Name: " + user.getName());
            //     System.out.print("  id: " + user.getWorker_id());

            //}
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }

        return room_types;
    }
}
