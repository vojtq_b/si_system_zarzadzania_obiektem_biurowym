package SZOB.ViewsControlers;

import SZOB.Models.Role;
import SZOB.Models.Room_Type;
import SZOB.Models.User;
import SZOB.ModelsControlers.DataValidator;
import SZOB.ModelsControlers.RoleController;
import SZOB.ModelsControlers.Room_TypeController;
import SZOB.ModelsControlers.UserController;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.*;
import javafx.stage.Stage;


import java.util.List;
import java.util.ResourceBundle;

/**
 * Created by Ulek on 2016-06-05.
 */
public class Room_TypeEditDialogController implements Initializable {

    public Label type_name_label;
    public Label price_label;
    public TextField price;
    private Room_Type room_type;
    private Stage dialogStage;
    public ChoiceBox role_box;
    Role choosenRole =null;

    private ObservableList<Role> roleData = FXCollections.observableArrayList();
    public void initialize(java.net.URL location,ResourceBundle resources) {
        RoleController roleController = new RoleController();




    }

    public void save_button_click(ActionEvent actionEvent) throws Exception{


       // if (    DataValidator.validateName(price.getText().trim()) == DataValidator.SUCCESS) {

            Room_TypeController room_typeController = new Room_TypeController();

            room_type.setPrice(Float.valueOf(price.getText().toString()));
            room_typeController.updateRoom_Type(room_type);
            dialogStage.close();
        //}

    }

    public void cancel_button_click(ActionEvent actionEvent) throws Exception{
        dialogStage.close();

    }

    public void setRoom_type(Room_Type room_type){
        if(room_type!=null) {

            this.room_type=room_type;
            type_name_label.setText(room_type.getType_name());
            price.setText(Float.toString(room_type.getPrice()));
        }
    }

    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }
}
