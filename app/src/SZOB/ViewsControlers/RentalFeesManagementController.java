package SZOB.ViewsControlers;
import SZOB.Models.Room_Type;
import SZOB.Models.User;
import SZOB.Models.Role;
import SZOB.ModelsControlers.RoleController;
import SZOB.ModelsControlers.Room_TypeController;
import SZOB.ModelsControlers.UserController;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.*;
import javafx.scene.*;
import javafx.fxml.FXMLLoader;
import org.hibernate.annotations.Table;
import org.omg.CORBA.PUBLIC_MEMBER;

import javax.swing.text.*;
import java.io.IOException;
import java.net.URI;
import java.util.Observable;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import java.util.List;

import javafx.scene.layout.AnchorPane;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
/**
 * Created by Ulek on 2016-06-04.
 */
public class RentalFeesManagementController implements Initializable{

    public Button pokaz;
    public TableView<Room_Type> room_typeTable;
    public TableColumn<Room_Type,Float> column_price;
    public TableColumn<Room_Type,String> column_type_name;
    public Label price_label;
    public Label type_name_label;
    private Room_Type ChoosenRoom_Type = null;

    private ObservableList<Room_Type> room_typeData = FXCollections.observableArrayList();
    Room_TypeController room_typecontroller = new Room_TypeController();
    RoleController rolecontroller = new RoleController();




    public void initialize(java.net.URL location,ResourceBundle resources) {
        this.updateTable();


        room_typeTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Room_Type>() {
            @Override
            public void changed(ObservableValue<? extends Room_Type> observable, Room_Type oldValue, Room_Type newValue) {
                showRoom_TypeDetails(newValue);
                ChoosenRoom_Type = newValue;
            }
        });


    }


    public void edit_room_type_button_click(ActionEvent actionEvent) throws Exception{
        showRoom_TypeEditDialog(ChoosenRoom_Type,actionEvent);

    }

    public void showRoom_TypeEditDialog(Room_Type room_type,ActionEvent actionEvent){

        Parent root;
        try {
            FXMLLoader loader =new FXMLLoader(getClass().getResource("../views/Room_TypeEditDialog.fxml"));
            root = loader.load();
            Stage stage = new Stage();
            stage.setTitle("Edycja ceny pokoju");
            stage.initOwner(((Node)(actionEvent.getSource())).getScene().getWindow());
            stage.initModality(Modality.WINDOW_MODAL);
            stage.setScene(new Scene(root));


            Room_TypeEditDialogController editcontroller = loader.getController();
            editcontroller.setDialogStage(stage);
            editcontroller.setRoom_type(ChoosenRoom_Type);
            stage.showAndWait();
            updateTable();
            //((Node)(actionEvent.getSource())).getScene().getWindow().hide();

        } catch (IOException e) {
            e.printStackTrace();
        }



    }

    public void updateTable(){
        column_type_name.setCellValueFactory(new PropertyValueFactory<Room_Type,String>("type_name"));
        column_price.setCellValueFactory(new PropertyValueFactory<Room_Type,Float>("price"));
        room_typeData=FXCollections.observableArrayList((List<Room_Type>) room_typecontroller.listRoom_Type());
        room_typeTable.setItems(room_typeData);

    }

    private void showRoom_TypeDetails(Room_Type room_type){
        if(room_type!=null) {

            price_label.setText(Float.toString(room_type.getPrice()));
            type_name_label.setText(room_type.getType_name());
        }
        else {

            price_label.setText("");
           type_name_label.setText("");

        }

    }

}


