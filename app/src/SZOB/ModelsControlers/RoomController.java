package SZOB.ModelsControlers;

import SZOB.Models.Room;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

/**
 * Created by Wojciech Altenberg on 2016-04-21.
 */
public class RoomController {

    public List<Room> listRoom( ){
        Session session = HibernateUtil.getSessionFactory().openSession();
        List rooms=null;
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            rooms = session.createQuery("FROM Room ").list();

            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }

        return rooms;
    }

    public static Room getRoom(int room_number) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        Room rt = null;
        try {
            tx = session.beginTransaction();
            rt = (Room) session.get(Room.class, room_number);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
            return rt;
        }
    }

}
