package SZOB.ViewsControlers;

import javafx.scene.control.Alert;

/**
 * Created by Lily on 17.04.2016.
 */
public abstract class WarningDialogController {
    public static void displayErrorDialog(String contentText) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("ERROR");
        alert.setHeaderText(null);
        alert.setContentText(contentText);
        alert.showAndWait();
    }
}
