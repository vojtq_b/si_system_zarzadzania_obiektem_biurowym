package SZOB.Models;

/**
 * Representation machines to get, set for role parameters (id and name)
 */
public class Role {
    private int role_id;
    private String role_name;

    /**
     * Constructor without parameters
     */
    public Role(){}

    /**
     * Constructor with two parameters with possible assign role parameters
     * @param role_id id of the role
     * @param role_name name of the role
     */
    public Role(int role_id, String role_name) {
        // this.role_id = role_id;
        this.role_name = role_name;
    }

    /**
     * method for show name of the role
     * @return name of the role
     */
    public String toString(){
        return role_name;
    }

    /**
     * method for show id of the role
     * @return id of the role
     */
    public int getRole_id() {
        return role_id;
    }

    /**
     * method for assign id of the role
     * @param role_id id of the role
     */
    public void setRole_id(int role_id) {
        this.role_id = role_id;
    }

    /**
     * method for show name of the role
     * @return name of the role
     */
    public String getRole_name() {
        return role_name;
    }

    /**
     * method for assign name of the role
     * @param role_name name of the role
     */
    public void setRole_name(String role_name) {
        this.role_name = role_name;
    }
}
