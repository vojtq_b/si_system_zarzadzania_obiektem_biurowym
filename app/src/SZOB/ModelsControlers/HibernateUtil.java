package SZOB.ModelsControlers;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;
import org.hibernate.Session;

public class HibernateUtil {
    private static final SessionFactory ourSessionFactory;
    private static final ServiceRegistry serviceRegistry;

    static {
        try {
            Configuration configuration = new Configuration();
            configuration.configure();

            serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();
            ourSessionFactory = configuration.buildSessionFactory(serviceRegistry);
        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory() throws HibernateException {
        return ourSessionFactory;
    }

    public static boolean IsDbAvailable(){
        boolean available = false;
        Session session = null;

        try
        {
             session= getSessionFactory().openSession(); //gets a new session from the session factory
            available = session.isConnected();
        }
        catch(Exception ex)
        {
            //add entry to application log
        }
        finally
        {
            if (null != session)
                session.close();
        }

        return available;
    }
  }


