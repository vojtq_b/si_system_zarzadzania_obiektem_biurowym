CREATE TABLE room_types
(
type_name VARCHAR(255) NOT NULL,
price FLOAT,
PRIMARY KEY(type_name)
);


CREATE TABLE rooms
(
room_number  INT NOT NULL,
room_area FLOAT,
room_type VARCHAR(255) NOT NULL,
PRIMARY KEY(room_number),
FOREIGN KEY(room_type) REFERENCES room_types(type_name)
);




CREATE TABLE reservations
(
reservation_id  BIGINT NOT NULL AUTO_INCREMENT,
tenant_name VARCHAR(255) NOT NULL,
tenant_id VARCHAR(255) NOT NULL,
start_date DATE NOT NULL,
end_date DATE NOT NULL,
room_number  INT NOT NULL,
worker_id  INT NOT NULL,
reservation_type INT,
PRIMARY KEY(reservation_id),
FOREIGN KEY(room_number) REFERENCES rooms(room_number),
FOREIGN KEY(worker_id) REFERENCES users(worker_id)
);