package SZOB.ViewsControlers;

import SZOB.Models.Role;
import SZOB.Models.User;
import SZOB.ModelsControlers.DataValidator;
import SZOB.ModelsControlers.RoleController;
import SZOB.ModelsControlers.UserController;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.util.List;
import java.util.ResourceBundle;

/**
 * Created by Ulek 2016-06-04.
 */
public class RentalFeesEditDialogController implements Initializable {

    public Label worker_id_label;
    public TextField name_text;
    public TextField last_name_text;
    public TextField email_text;
    public TextField tel_nember_text;
    private User user;
    private Stage dialogStage;
    public ChoiceBox role_box;
    Role choosenRole =null;

    private ObservableList<Role> roleData = FXCollections.observableArrayList();
    public void initialize(java.net.URL location,ResourceBundle resources) {
        RoleController roleController = new RoleController();

        roleData= FXCollections.observableArrayList((List<Role>) roleController.listRoles());
        //role_box.setS
        role_box.setItems(roleData);
        role_box.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Role>() {
            @Override
            public void changed(ObservableValue<? extends Role> observable, Role oldValue, Role newValue) {
                choosenRole=newValue;
            }
        });

    }

    public void save_button_click(ActionEvent actionEvent) throws Exception{


        if (    DataValidator.validateName(name_text.getText().trim()) == DataValidator.SUCCESS &&
                DataValidator.validateName(last_name_text.getText().trim()) == DataValidator.SUCCESS &&
                DataValidator.validateCellphoneNumber(tel_nember_text.getText().trim()) == DataValidator.SUCCESS &&
                DataValidator.validateEmail(email_text.getText().trim()) == DataValidator.SUCCESS) {

            UserController userControler = new UserController();

            user.setName(name_text.getText().trim());
            user.setLast_name(last_name_text.getText().trim());
            user.setE_mail(email_text.getText().trim());
            user.setTel_number(tel_nember_text.getText().trim());
            user.setRole_id(choosenRole.getRole_id());
            userControler.updateUser(user);
            dialogStage.close();
        }

    }

    public void cancel_button_click(ActionEvent actionEvent) throws Exception{
        dialogStage.close();

    }

    public void setUser(User user){
        if(user!=null) {

            this.user=user;
            worker_id_label.setText(Integer.toString(this.user.getWorker_id()));
            name_text.setText(this.user.getName());
            last_name_text.setText(this.user.getLast_name());
            email_text.setText(this.user.getE_mail());
            tel_nember_text.setText(this.user.getTel_number());
            role_box.getSelectionModel().select(this.user.getRole_id());
        }
    }

    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }
}
