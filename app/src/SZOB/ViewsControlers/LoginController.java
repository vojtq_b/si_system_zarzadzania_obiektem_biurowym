package SZOB.ViewsControlers;

import SZOB.ModelsControlers.HibernateUtil;
import SZOB.ModelsControlers.UserController;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.stage.*;
import javafx.scene.*;
import javafx.fxml.FXMLLoader;

import javafx.scene.Parent;
import javafx.scene.control.Alert.*;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Wojtek on 2016-04-03.
 */
public class LoginController implements Initializable{
    public Button LoginButton;
    public TextField UserId_Text_Field;
    public PasswordField PasswordField;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        HibernateUtil.getSessionFactory();

    }


    public static Stage homestage;
    public void login_click(ActionEvent actionEvent) throws Exception {
        if (true ) {




            if (UserController.Login(Integer.valueOf(UserId_Text_Field.getText()), PasswordField.getText()) == 1) {


                Parent home = FXMLLoader.load(getClass().getResource("../views/Main.fxml"));
                Scene homescene = new Scene(home);
                Stage appstage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
                homestage=appstage;
                appstage.setScene(homescene);
                appstage.show();
            } else {

                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("ERROR");
                alert.setHeaderText("Bład logowanie");
                alert.setContentText("Nie poprawny numer pracownika lub haslo");
                alert.showAndWait();

                System.out.println("zle");
            }

        }
        else {
            System.out.println("brak polaczenia");
        }
    }
}
