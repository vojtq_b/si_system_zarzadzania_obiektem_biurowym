package SZOB.ModelsControlers;

import SZOB.Models.Reservation;

import SZOB.Models.Room;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import SZOB.ModelsControlers.HibernateUtil;
import org.hibernate.criterion.Restrictions;

import java.time.Duration;
import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;

public class ReservationController {
	
	//Add Reservation
     public Long addReservation(Reservation reservation){
        Session session = HibernateUtil.getSessionFactory().openSession();
        reservation.setWorker_id(UserController.getActiveUser().getWorker_id());
        Transaction tx = null;
        Long ReservationID=null;
 
        try{
            tx = session.beginTransaction();
            ReservationID=(Long) session.save(reservation);
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
        return ReservationID;
    }
	
	//Delete Reservation
    public void deleteReservation(Reservation reservation){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;

        try{
            tx = session.beginTransaction();
            // Role rr = (Role)session.load(Role.class,user.getRole_id().getRole_id());
            // user.setRole_id(rr);
            session.delete(reservation);
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }

    }

    public static List<Reservation> listReservations( ){
        Session session = HibernateUtil.getSessionFactory().openSession();
        List reservations=null;
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            reservations = session.createQuery("FROM Reservation ").list();

            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }

        return reservations;
    }
	
	public static List<Reservation> ListReservationInRoom(int room_number){
        Session session = HibernateUtil.getSessionFactory().openSession();
        List reservations=null;
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
 
            Criteria cr=session.createCriteria(Reservation.class);
            cr.add(Restrictions.eq("room_number",room_number));
            reservations = cr.list();
            //reservations = session.createQuery("FROM Reservation ").list();
 
 
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
 
        return reservations;
    }

    public static List<Reservation> ListReservationInRoomNameDate(int room_number, Date start, Date end, String tenant,int archived){
        Session session = HibernateUtil.getSessionFactory().openSession();
        List reservations=null;
        Transaction tx = null;
        try{
            tx = session.beginTransaction();

            Criteria cr=session.createCriteria(Reservation.class);
            cr.add((Restrictions.eq("archived",archived)));
            if(room_number>0) {
                cr.add(Restrictions.eq("room_number", room_number));
            }
            if(tenant.trim().length()>0){
                cr.add(Restrictions.like("tenant_name",tenant.trim(), MatchMode.START));
            }
            if(start!=null){
                cr.add(Restrictions.ge("start_date",start));
            }
            if(end!=null){
                cr.add(Restrictions.le("end_date",end));

            }
            reservations = cr.list();
            //reservations = session.createQuery("FROM Reservation ").list();


            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }

        return reservations;
    }


    public long getDuration(LocalDate start, LocalDate end, int type){

        if(type==1){
            try {
               // return Duration.between(start.atStartOfDay(), end.atStartOfDay()).toDays()+1;
                return ChronoUnit.DAYS.between(start,end)+1;
            }catch (java.time.temporal.UnsupportedTemporalTypeException e){
                return 0;
            }
        }
        if (type==0){
            try {

                Period period = Period.between(start,end);
                ChronoUnit.MONTHS.between(start,end);
                return (long) (ChronoUnit.MONTHS.between(start,end))+1;
            }catch (java.time.temporal.UnsupportedTemporalTypeException e){
                return 0;
            }
        }
        return 0;
    }

    public float getTotalCost(long duration, int type, Room room){
        Room_TypeController room_typeController = new Room_TypeController();

        if(room.getRoom_type().equals("Biuro")) {
            if (type == 0) return room_typeController.getPrice(room.getRoom_type()) * room.getRoom_area();
            if (type == 1) {
                return room_typeController.getPrice(room.getRoom_type()) / 30 * duration * room.getRoom_area();
            }
        }
        if(room.getRoom_type().equals("Sala Konferencyjna")) {
            if (type == 0) return room_typeController.getPrice(room.getRoom_type())*30 * room.getRoom_area();
            if (type == 1) {
                return room_typeController.getPrice(room.getRoom_type()) * duration * room.getRoom_area();
            }
        }

        return 0;
    }

    public void updateReservation(Reservation reservation){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;

        try{
            tx = session.beginTransaction();

            session.update(reservation);
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
    }

    public void archiveReserwations(){
        List<Reservation> reservations=null;
        Date date = new Date();
        reservations =ListReservationInRoomNameDate(0,null,date,"",0);

        for(Reservation x:reservations){
            x.setArchived(1);
            updateReservation(x);
        }



    }
	
}