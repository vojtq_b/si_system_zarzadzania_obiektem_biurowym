

package SZOB.ModelsControlers;

import SZOB.Models.Role;
import SZOB.Models.User;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import SZOB.ModelsControlers.HibernateUtil;
import java.util.Iterator;
import java.util.List;
/**
 * Created by Admin on 2016-04-13.
 */

public class UserController {
    private static User ActiveUser;

    public static User getActiveUser() {
        return ActiveUser;
    }

    //Add User
    public int addUser(User user){
        Session session = HibernateUtil.getSessionFactory().openSession();

        Transaction tx = null;
        Integer UserID=null;

        try{
            tx = session.beginTransaction();
            //session.save(user.getRole_id());
            // Role rr = (Role)session.load(Role.class,user.getRole_id().getRole_id());
            // user.setRole_id(rr);
            UserID=(Integer) session.save(user);
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
        return UserID;
    }

    //Update User
    public void updateUser(User user){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        Integer UserID=null;

        try{
            tx = session.beginTransaction();
            //Role rr = (Role)session.load(Role.class,user.getRole_id().getRole_id());
            //user.setRole_id(rr);
            session.update(user);
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
    }

    //Delete User
    public void deleteUser(User user){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;

        try{
            tx = session.beginTransaction();
            // Role rr = (Role)session.load(Role.class,user.getRole_id().getRole_id());
            // user.setRole_id(rr);
            session.delete(user);
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }

    }
	
    public List<User> listUsers( ){
        Session session = HibernateUtil.getSessionFactory().openSession();
        List users=null;
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            users = session.createQuery("FROM User").list();
           // for (Iterator iterator =
          //       users.iterator(); iterator.hasNext();){
           //     User user = (User) iterator.next();
           //     System.out.print("First Name: " + user.getName());
           //     System.out.print("  id: " + user.getWorker_id());

            //}
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }

        return users;
    }
    
	//Login User
    public static int Login(int worker_id, String password){
        int  logged=0;
        Session session = HibernateUtil.getSessionFactory().openSession();

        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            User user = (User) session.get(User.class, worker_id);
            if(user==null)
            {
                return logged;
            }
            else{
                if (user.getPassword().equals(password)) {
                    ActiveUser = user;
                    logged = 1;
                }
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return logged;
    }
	
	 

}
