package SZOB.Models;

/**
 * Created by Dominik on 2016-04-19.
 */
public class Room {
    private int room_number;
    private float room_area;
    private String room_type;

    public Room() {}

    public Room(int room_number, float room_area, String room_type){
        this.room_number=room_number;
        this.room_area=room_number;
        this.room_type=room_type;
    }

    public int getRoom_number(){
        return room_number;
    }

    public void setRoom_number(int room_number){
        this.room_number=room_number;
    }

    public float getRoom_area(){
        return room_area;
    }

    public void setRoom_area(float room_area){
        this.room_area=room_area;
    }

    public String getRoom_type(){
        return room_type;
    }

    public void setRoom_type(String room_type){
        this.room_type=room_type;
    }

    @Override
    public String toString() {
        if(room_number==0){
            return room_type;

        }
        return Integer.toString(room_number)+" "+room_type;
    }
}
