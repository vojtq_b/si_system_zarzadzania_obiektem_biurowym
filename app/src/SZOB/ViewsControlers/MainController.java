package SZOB.ViewsControlers;
import SZOB.Models.Acces;
import SZOB.ModelsControlers.AccesController;
import SZOB.ModelsControlers.ReservationController;
import SZOB.ModelsControlers.UserController;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.stage.*;
import javafx.scene.*;
import javafx.fxml.FXMLLoader;

import java.io.IOException;
import java.net.URI;
import java.util.ResourceBundle;

/**
 * Created by Wojtek on 2016-04-08.
 */
public class MainController implements Initializable{

    public TabPane mainTabPene;

    public void initialize(java.net.URL location,ResourceBundle resources)
    {
        ReservationController reservationController =new ReservationController();
        reservationController.archiveReserwations();
        if(AccesController.hasAcces(UserController.getActiveUser(),0)){
            addTab("Zarządzanie użytkownikami","usermanagement_tab","../views/UserManagement.fxml");
        }

        if(AccesController.hasAcces(UserController.getActiveUser(),2)) {
            addTab("Zarządzanie rezerwacjami", "reservation_management", "../views/ReservationManagement.fxml");
        }

        if(AccesController.hasAcces(UserController.getActiveUser(),4)) {
            addTab("Zarządzanie opłatami najmu", "rental_fees_management", "../views/RentalFeesManagement.fxml");
        }
        if(AccesController.hasAcces(UserController.getActiveUser(),5)) {
            addTab("Archiwum", "archive", "../views/ArchiveReservation.fxml");
        }
        mainTabPene.getSelectionModel().selectFirst();
          }

    public void tab1acction(ActionEvent actionEvent) throws Exception{


    }

    private void addTab(String title,String id,String view_path){
        Tab newTab = new Tab();
        newTab.setText(title);
        newTab.setClosable(false);
        newTab.setId(id);

        try {
            newTab.setContent(FXMLLoader.load(getClass().getResource(view_path)));
            mainTabPene.getTabs().add(newTab);
        }catch (IOException e){

        }
    }
}
