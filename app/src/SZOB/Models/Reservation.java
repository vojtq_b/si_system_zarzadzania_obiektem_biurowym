package SZOB.Models;

import java.util.Date;

/**
 * Created by Dominik on 2016-04-19.
 */

/**
 * Representation machines to get, set for reservation parameters
 */
public class Reservation {
    private long reservation_id;
    private String tenant_name;
    private String tenant_id;
    private Date start_date;
    private Date end_date;
    private int room_number;
    private int worker_id;
    private int reservation_type;
    private float price;
    private int archived;

    public int getArchived() {
        return archived;
    }

    public void setArchived(int archived) {
        this.archived = archived;
    }

    /**
     * Constructor without parameters
     */
    public Reservation() {}

    /**
     * Constructor with two parameters with possible assign date
     * @param start_date starting the day the reservation
     * @param end_date ending the day the reservation
     */
    public Reservation(Date start_date, Date end_date) {
        this.start_date = start_date;
        this.end_date = end_date;
    }

    /**
     * Constructor with all parameters with possible ascribe all reservation
     * @param tenant_name the name of tenant
     * @param tenant_id the id of tenant
     * @param start_date the first day of the reservation
     * @param end_date the last day of the reservation
     * @param room_number room number
     * @param worker_id the worker id
     * @param reservaation_type type of the reservation(long or short)
     */
    public Reservation( String tenant_name, String tenant_id, Date start_date, Date end_date, int room_number, int worker_id, int reservaation_type) {

        this.tenant_name = tenant_name;
        this.tenant_id = tenant_id;
        this.start_date = start_date;
        this.end_date = end_date;
        this.room_number = room_number;
        this.worker_id = worker_id;
        this.reservation_type = reservaation_type;
        this.archived=0;
    }

    /**
     * method for show number of the reservation
     * @return number reservation
     */
    public long getReservation_id() {
        return reservation_id;
    }

    /**
     * method for assign number of the reservation
     * @param reservation_id number of the reservation
     */
    public void setReservation_id(long reservation_id) {
        this.reservation_id = reservation_id;
    }

    /**
     * method for show name of tenant
     * @return name of tenant
     */
    public String getTenant_name() {
        return tenant_name;
    }

    /**
     * method for assign name of tenant
     * @param tenant_name name of tenant
     */
    public void setTenant_name(String tenant_name) {
        this.tenant_name = tenant_name;
    }

    /**
     * method for show id of tenant
     * @return id of tenant
     */
    public String getTenant_id() {
        return tenant_id;
    }

    /**
     * method for assign id of tenant
     * @param tenant_id id of tenant
     */
    public void setTenant_id(String tenant_id) {
        this.tenant_id = tenant_id;
    }

    /**
     * method for show the first day of the reservation
     * @return the first day of the reservation
     */
    public Date getStart_date() {
        return start_date;
    }

    /**
     * method for assign the first day of the reservation
     * @param start_date the first day of the reservation
     */
    public void setStart_date(Date start_date) {
        this.start_date = start_date;
    }

    /**
     * method for show the last day of the reservation
     * @return the last day of the reservation
     */
    public Date getEnd_date() {
        return end_date;
    }

    /**
     * method for assign the last day of the reservation
     * @param end_date the last day of the reservation
     */
    public void setEnd_date(Date end_date) {
        this.end_date = end_date;
    }

    /**
     * method for show the room number
     * @return the room number
     */
    public int getRoom_number() {
        return room_number;
    }

    /**
     * method for assign the room number
     * @param room_number the room number
     */
    public void setRoom_number(int room_number) {
        this.room_number = room_number;
    }

    /**
     * method for show the worker id
     * @return the worker id
     */
    public int getWorker_id() {
        return worker_id;
    }

    /**
     * method for assign the worker id
     * @param worker_id the worker id
     */
    public void setWorker_id(int worker_id) {
        this.worker_id = worker_id;
    }

    /**
     * method for show type of the reservation
     * @return type of the reservation
     */
    public int getReservation_type() {
        return reservation_type;
    }

    /**
     * method for assign type of the reservation
     * @param reservaation_type type of the reservation
     */
    public void setReservation_type(int reservaation_type) {
        this.reservation_type = reservaation_type;
    }

    /**
     * method for show price of the reservation
     * @return price of the reservation
     */
    public float getPrice() {
        return price;
    }

    /**
     * method for assign price of the reservation
     * @param price price of the reservation
     */
    public void setPrice(float price) {
        this.price = price;
    }
}

