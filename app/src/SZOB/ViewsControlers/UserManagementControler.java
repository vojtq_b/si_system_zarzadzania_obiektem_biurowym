package SZOB.ViewsControlers;
import SZOB.Models.User;
import SZOB.Models.Role;
import SZOB.ModelsControlers.RoleController;
import SZOB.ModelsControlers.UserController;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.*;
import javafx.scene.*;
import javafx.fxml.FXMLLoader;
import org.hibernate.annotations.Table;
import org.omg.CORBA.PUBLIC_MEMBER;

import javax.swing.text.*;
import java.io.IOException;
import java.net.URI;
import java.util.Observable;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import java.util.List;

import javafx.scene.layout.AnchorPane;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
/**
 * Created by Wojtek on 2016-04-08.
 */
public class UserManagementControler implements Initializable{

    public Button pokaz;
    public TableView<User> userTable;
    public TableColumn<User,String> collumn_worker_id;
    public TableColumn<User,String> collumn_name;
    public TableColumn<User,String> collumn_last_name;
    public Label worker_id_lable;
    public Label role_lable;
    public Label name_lable;
    public Label last_name_lable;
    public Label tel_number_lable;
    public Label email_lable;
    public Button delete_user_button;
    private User ChoosenUser =null;

    private ObservableList<User> userData = FXCollections.observableArrayList();
    UserController usercontroler =new UserController();
    RoleController rolecontroller =new RoleController();




    public void initialize(java.net.URL location,ResourceBundle resources) {
        this.updateTable();


        userTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<User>() {
            @Override
            public void changed(ObservableValue<? extends User> observable, User oldValue, User newValue) {
                showUserDetails(newValue);
                ChoosenUser=newValue;
            }
        });


    }




    public void delete_user_button_click(ActionEvent actionEvent) throws Exception{
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Uwaga");
        String s = "Czy napewno chcesz usunąć użytkownika";
        alert.setContentText(s);
        alert.setHeaderText(null);
        Optional<ButtonType> result = alert.showAndWait();

        if ((result.isPresent()) && (result.get() == ButtonType.OK)) {

            if (ChoosenUser != null) {
                usercontroler.deleteUser(ChoosenUser);
                updateTable();
            }
        }



    }

    public void edit_user_button_click(ActionEvent actionEvent) throws Exception{
        showUserEditDialog(ChoosenUser,actionEvent);

    }

    public void add_user_button_click(ActionEvent actionEvent) throws Exception{
        Parent root;
        try {
            FXMLLoader loader =new FXMLLoader(getClass().getResource("../views/UserCreateDialog.fxml"));
            root = loader.load();
            Stage stage = new Stage();
            stage.setTitle("Dodawanie pracownika");
            stage.initOwner(((Node)(actionEvent.getSource())).getScene().getWindow());
            stage.initModality(Modality.WINDOW_MODAL);
            stage.setScene(new Scene(root));


            UserCreateDialogController ccreatecontroller = loader.getController();
            ccreatecontroller.setDialogStage(stage);
            stage.showAndWait();
            updateTable();

            //((Node)(actionEvent.getSource())).getScene().getWindow().hide();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void showUserEditDialog(User user,ActionEvent actionEvent){

        Parent root;
        try {
            FXMLLoader loader =new FXMLLoader(getClass().getResource("../views/UserEditDialog.fxml"));
            root = loader.load();
            Stage stage = new Stage();
            stage.setTitle("Edycja pracownika");
            stage.initOwner(((Node)(actionEvent.getSource())).getScene().getWindow());
            stage.initModality(Modality.WINDOW_MODAL);
            stage.setScene(new Scene(root));


            UserEditDialogController editcontroller = loader.getController();
            editcontroller.setUser(user);
            editcontroller.setDialogStage(stage);
            stage.showAndWait();
            updateTable();
            //((Node)(actionEvent.getSource())).getScene().getWindow().hide();

        } catch (IOException e) {
            e.printStackTrace();
        }


    }
    public void updateTable(){
        collumn_worker_id.setCellValueFactory(new PropertyValueFactory<User,String>("worker_id"));
        collumn_name.setCellValueFactory(new PropertyValueFactory<User,String>("name"));
        collumn_last_name.setCellValueFactory(new PropertyValueFactory<User,String>("last_name"));
        userData=FXCollections.observableArrayList((List<User>) usercontroler.listUsers());
        userTable.setItems(userData);

    }

    private void showUserDetails(User user){
        if(user!=null) {

            worker_id_lable.setText(Integer.toString(user.getWorker_id()));
            role_lable.setText(rolecontroller.getRole(user.getRole_id()).getRole_name());
            name_lable.setText(user.getName());
            last_name_lable.setText(user.getLast_name());
            tel_number_lable.setText(user.getTel_number());
            email_lable.setText(user.getE_mail());
        }
        else {

            worker_id_lable.setText("");
            role_lable.setText("");
            name_lable.setText("");
            last_name_lable.setText("");
            tel_number_lable.setText("");
            email_lable.setText("");
        }

    }

}


