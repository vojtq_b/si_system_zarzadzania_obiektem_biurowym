package SZOB.Reports;

import SZOB.Models.Reservation;
import javafx.collections.ObservableList;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Jarosław on 2016-06-08.
 */
public class archiveRaportCreator {
    public void CreatePDF(ObservableList<Reservation> list) throws IOException {
        Date currentDate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
        String fileName = sdf.format(currentDate) + ".pdf";
        try {
            PDDocument doc = new PDDocument();
            PDPage page = new PDPage();

            doc.addPage(page);

            PDPageContentStream content = new PDPageContentStream(doc, page);

            String[] table = {"Nazwa najemcy: ","Pokój: ","Data rozpoczecia wynajmu: ","Data zakonczenia wynajmu: ", "Cena za wynajem pokoju: "} ;

            int margin = 10;
            int y1 = 780;
            int y2 = 780;
            int length = 295;
            int breadth = 30;

            for (int i = 0; i < list.size(); i++) {

                String[] parameters = {list.get(i).getTenant_name(),
                        String.valueOf(list.get(i).getRoom_number()),
                        String.valueOf(list.get(i).getStart_date()),
                        String.valueOf(list.get(i).getEnd_date()),
                        String.valueOf(list.get(i).getPrice())};

                for (int j = 0; j < table.length; j++) {//ilość krawedzi w kolumnie
                    content.drawLine(margin, y1 , margin, y2-breadth);
                    content.drawLine(length,y1 , length, y2-breadth);
                    content.drawLine(length+length,y1 , length+length, y2-breadth);

                    String stringTable = table[j];
                    String parametersTable = parameters[j];
                    content.setFont(PDType1Font.COURIER, 12);
                    content.beginText();
                    content.moveTextPositionByAmount(margin + 5, y1 - 15);
                    content.drawString(stringTable);
                    content.moveTextPositionByAmount(length + 5, 0);
                    content.drawString(parametersTable);
                    content.endText();

                    y2-=breadth;
                    y1=y2;
                }
                y2 -= breadth;
                y1 = y2;
            }
            y1 = 780;
            y2 = 780;
            for (int i = 0; i < list.size(); i++) {
                for (int j = 0; j < 5; j++) {
                    content.drawLine(margin, y1 , length*2, y2);
                    y2-=breadth;
                    y1=y2;
                }
            }

            content.close();
            doc.save(fileName);

        }catch(Exception e)
        {
            e.printStackTrace();
        }


    }
}
