package SZOB.Reports;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

/**
 * Created by Jarosław on 2016-05-17.
 */
public class currentReportCreator {

    public void CreatePDF(String name, LocalDate start_date, LocalDate end_date, String text, String room) {

        String[] parameters = {name, room, String.valueOf(start_date), String.valueOf(end_date), text};

        Date currentDate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
        String fileName = sdf.format(currentDate) + ".pdf";
        String[] table = {"Nazwa najemcy: ","Pokój: ","Data rozpoczecia wynajmu: ","Data zakonczenia wynajmu: ", "Cena za wynajem pokoju: "} ;

        int margin = 10;
        int y1 = 780;
        int y2 = 780;
        int y3 = 770;
        int length = 295;
        int breadth = 30;

        try{
            PDDocument doc = new PDDocument();
            PDPage page = new PDPage();

            doc.addPage(page);

            PDPageContentStream content = new PDPageContentStream(doc, page);

            for (int i = 0; i < table.length; i++) {//ilość krawedzi w kolumnie
                content.drawLine(margin, y1 , margin, y2-breadth);
                content.drawLine(length,y1 , length, y2-breadth);
                content.drawLine(length+length,y1 , length+length, y2-breadth);

                String stringTable = table[i];
                String parametersTable = parameters[i];

                content.setFont(PDType1Font.COURIER, 12);
                content.beginText();
                content.moveTextPositionByAmount(margin + 5, y1 - 15);
                content.drawString(stringTable);
                content.moveTextPositionByAmount(length + 5, 0);
                content.drawString(parametersTable);
                content.endText();

                y2-=breadth;
                y1=y2;
            }
            y1 = 780;
            y2 = 780;
            for (int i = 1; i <= 5; i++) {
                content.drawLine(margin, y1 , length*2, y2);
                y2-=breadth;
                y1=y2;
            }
            content.drawLine(margin, y1 , length*2, y2);

            content.close();
            doc.save(fileName);
            doc.close();
        }

        catch(Exception e){
            System.out.println(e);
        }
    }

}
